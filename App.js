/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { addValue, subtractValue } from './App/redux/actions/action'
import { store } from './App/redux/store/configureStore'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: store.getState().first.value,
      results: []
    };

  }

  componentDidMount() {
    fetch("https://itunes.apple.com/search?term=Michael+jackson", {
      method: "GET",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(async (response) => {
      let resp = await response.json();
      //alert(JSON.stringify(resp.results[0].trackId));
      var localArray = [];
      if (resp.results.length > 0) {
        resp.results.map((item, index) => {
          var localObject = item;
          localObject.key = index.toString();
          localArray.push(localObject);
        })
        this.setState({ results: localArray });

      }
    }).catch((err) => {
      alert(JSON.stringify(err));
    })
  }
  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', height: '100%', }}>
            <Text style={{ fontSize: 20, color: 'black' }}>Song List</Text>
            <FlatList
              style={{ flex: 1 }}
              data={this.state.results}
              ListEmptyComponent={<View><Text> No items</Text></View>}
              ItemSeparatorComponent={() => <View style={{ width: '100%', height: 1, backgroundColor: '#000' }}></View>}
              renderItem={({ item, index }) => (
                <View style={{ width: '85%', borderRadius: 10, padding: 10, backgroundColor: 'cyan', margin: 20, flexDirection: 'row' }}>
                  <Image source={{ uri: item.artworkUrl100 }} style={{ height: 50, width: 50 }} />
                  
                  <View style={{ marginLeft: 20 , flexDirection:'column'}}>
                    <Text  style={{ fontSize: 20, color: 'black',flexWrap: 'wrap', width:'100%' }} >{item.trackName}</Text>
                    <View style={{width:'70%', flexDirection:'row', justifyContent:'space-between'}}>
                    <Text style={{ fontSize: 20, color: 'black' }}>{item.artistName}</Text>
                    <View>
                    <Text style={{ fontSize: 20, color: 'black' }}>{item.trackTimeMillis}</Text>
                    <Text style={{ fontSize: 20, color: 'black' }}>{item.trackCount}</Text>
                    </View>
                    </View>
                  </View>
                 
                  {/* <Text style={{ fontSize: 20, color: 'black' }}>{JSON.stringify(item)}</Text> */}
                </View>
              )}
              keyExtractor={(item, index) => {
                return item.key;
              }}
            />

            {/* <TouchableOpacity
              style={{ height: 50, length: 100, backgroundColor: 'grey', borderRadius: 10, justifyContent: 'center' }}
              onPress={() => {
                store.dispatch(addValue(this.state.value))
                alert(JSON.stringify(store.getState().first.value));
              }}
            >
              <Text style={{ fontSize: 20, color: 'blue' }}>increase</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ height: 50, length: 100, backgroundColor: 'grey', borderRadius: 10, justifyContent: 'center' }}
              onPress={() => {
                store.dispatch(subtractValue(this.state.value))
              }}
            ><Text style={{ fontSize: 20, color: 'red' }}>decrease</Text>
            </TouchableOpacity> */}
          </View>

        </SafeAreaView>
      </>
    );
  }

}

// const App: () => React$Node = () => {
//   
// };

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});