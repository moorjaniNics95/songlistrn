import {Provider, connect} from 'react-redux'
import {applyMiddleware, createStore, compose} from 'redux'
var thunkMiddleware = require('redux-thunk').default
import rootReducer from '../reducers/rootReducer'

export const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));