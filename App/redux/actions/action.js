export const addValue = (inputValue) => ({
    type: 'ADD_VALUE',
    inputValue,
});

export const subtractValue = (inputValue) => ({
    type: 'SUBTRACT_VALUE',
    inputValue,
});