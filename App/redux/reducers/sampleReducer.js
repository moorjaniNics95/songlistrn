const INITIAL_STATE= {
    value: 0
};

const sampleReducer= (state= INITIAL_STATE, action) => {
    switch(action.type) {
        case 'ADD_VALUE':
            return{...state, value: action.value + 1};
        case 'SUBTRACT_VALUE':
            return{...state, value: action.value - 1};
        default: 
        return state;
    }
}

export default sampleReducer;